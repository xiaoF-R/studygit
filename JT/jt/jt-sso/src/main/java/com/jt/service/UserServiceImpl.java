package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements  UserService{

    private static Map<Integer,String> paramMap = new HashMap<>();
    static {
        paramMap.put(1, "username");
        paramMap.put(2, "phone");
        paramMap.put(3, "email");
    }


    @Autowired
    private UserMapper userMapper;


    @Override
    public List<User> findAll() {

        return userMapper.selectList(null);
    }

    /**
     * 校验数据是否存在即可. 查询总记录数即可.
     * @param param   需要校验的数据
     * @param type    校验的类型 1username 2phone 3 email 6789910
     * @return
     */
    @Override
    public Boolean checkUser(String param, Integer type) {
        String column = paramMap.get(type);
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(column, param);
        //boolean flag = userMapper.selectCount(queryWrapper)>0?true:false;
        return userMapper.selectCount(queryWrapper)>0;
    }

    @Override
    public User findUserById(Long userId) {

        return userMapper.selectById(userId);
    }
}
