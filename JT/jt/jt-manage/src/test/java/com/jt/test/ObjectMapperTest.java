package com.jt.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.pojo.ItemDesc;
import com.jt.util.ObjectMapperUtil;
import org.junit.jupiter.api.Test;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ObjectMapperTest {

    //实现对象与JSON之间的转化
    //任务: 对象转化为json
    @Test
    public void test01(){
        ObjectMapper objectMapper = new ObjectMapper();
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).setItemDesc("json测试")
                .setCreated(new Date()).setUpdated(new Date());
        try {
            //1.将对象转化为json
           String result =  objectMapper.writeValueAsString(itemDesc);
            System.out.println(result);
           //2.将json数据转化为对象 只能通过反射机制..
            ItemDesc itemDesc2 = objectMapper.readValue(result,ItemDesc.class);
            System.out.println(itemDesc2.toString());
            System.out.println(itemDesc2.getCreated());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test02(){
        ObjectMapper objectMapper = new ObjectMapper();
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).setItemDesc("json测试")
                .setCreated(new Date()).setUpdated(new Date());
        List<ItemDesc> list = new ArrayList<>();
        list.add(itemDesc);
        list.add(itemDesc);
        //1.将对象转化为JSON
        try {
            String json = objectMapper.writeValueAsString(list);
            System.out.println(json);
            //2.json转化为对象
            List<ItemDesc> list2 = objectMapper.readValue(json, list.getClass());
            System.out.println(list2);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test03(){
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).setItemDesc("json测试")
                .setCreated(new Date()).setUpdated(new Date());
        String json = ObjectMapperUtil.toJSON(itemDesc);
        ItemDesc itemDesc2 =
                ObjectMapperUtil.toObject(json, ItemDesc.class);
        System.out.println(json);
        System.out.println(itemDesc2);
    }
}
