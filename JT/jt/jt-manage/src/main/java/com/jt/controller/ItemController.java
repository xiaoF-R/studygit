package com.jt.controller;

import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.vo.EasyUITable;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jt.service.ItemService;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController	//返回值都是JSON数据
@RequestMapping("/item")
public class ItemController {
	
	@Autowired
	//@Qualifier("aaaa")
	private ItemService itemService;

	/**
	 * 业务需求:商品信息的分页查询.
	 * url地址: http://localhost:8091/item/query?page=1&rows=50
	 * 请求参数: page 页数 , rows 行数
	 * 返回值结果: EasyUITable
	 * 开发顺序: mapper~~service~~~controller~~页面  自下而上的开发
	 * 京淘开发顺序: 分析页面需求~~~~Controller~~~~Service~~~Mapper  自上而下的开发
	 *
	 * */
	@RequestMapping("/query")
	public EasyUITable findItemByPage(Integer page,Integer rows){

		return itemService.findItemByPage(page,rows);
	}

	/**
	 * 业务需求: 完成商品入库操作,返回系统vo对象
	 * url1: /item/save
	 * 参数: 整个form表单   itemDesc参数
	 * 返回值: SysResult对象
	 */
	@RequestMapping("save")
	public SysResult saveItem(Item item,ItemDesc itemDesc){
		//数据一起完成入库操作.
		itemService.saveItem(item,itemDesc);
		return SysResult.success();
		//全局异常的处理机制!!!!
		/*try {
			itemService.saveItem(item);
			return SysResult.success();
		}catch (Exception e){
			e.printStackTrace();
			return SysResult.fail();
		}*/
	}


	/**
	 * 业务说明: 商品修改操作
	 * url: /item/update
	 * 参数: 整个form表单(多个) 单个还是多个
	 * 返回值: SysResult对象
	 */
	@RequestMapping("/update")
	public SysResult updateItem(Item item,ItemDesc itemDesc){

		itemService.updateItem(item,itemDesc);
		return SysResult.success();
	}

	/**
	 * 商品删除
	 * url: /item/delete
	 * 参数: {"ids":"100,101,102"}    ids数据类型是字符串!!!
	 * 返回值: 系统返回值变量
	 *
	 * 注意事项: 取值与赋值操作的key必须相同!!!
	 */
	@RequestMapping("/delete")
	public SysResult deleteItems(Long... ids){

		itemService.deleteItems(ids);
		return SysResult.success();
	}

	/**
	 * 需求: 利用一个方法实现商品上架/下架操作    restFul风格实现
	 * url1:/item/updateStatus/2   status=2
	 * url2:/item/updateStatus/1	status=1
	 */
	@RequestMapping("/updateStatus/{status}")
	public SysResult updateStatus(@PathVariable Integer status,Long... ids){

		itemService.updateStatus(status,ids);
		return SysResult.success();
	}

	/**
	 * 根据ItemId查询ItemDesc对象
	 * url:http://localhost:8091/item/query/item/desc/1474391989
	 * 参数: restFul形式
	 * 返回值: SysResult对象
	 */
	@RequestMapping("/query/item/desc/{itemId}")
	public SysResult findItemDescById(@PathVariable Long itemId){

		ItemDesc itemDesc = itemService.findItemDescById(itemId);
		return SysResult.success(itemDesc); //200 返回业务数据
	}







}
