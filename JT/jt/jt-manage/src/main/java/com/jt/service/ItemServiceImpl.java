package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.ItemDescMapper;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.vo.EasyUITable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jt.mapper.ItemMapper;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService { //eclipse  alt+shift+p
													  //alt +insert
	
	@Autowired
	private ItemMapper itemMapper;
	@Autowired
	private ItemDescMapper itemDescMapper;

	/**
	 * 在进行分页查询时,MP必须添加配置类
	 * 利用MP机制,实现商品查询
	 * @param page
	 * @param rows
	 * @return
	 */
	@Override
	public EasyUITable findItemByPage(Integer page, Integer rows) {
		//查询条件根据更新时间进行排序.
		QueryWrapper<Item> queryWrapper = new QueryWrapper<>();
		queryWrapper.orderByDesc("updated");
		//当用户将参数传递之后,MP会自己执行分页操作后,将需要的数据进行封装.
		//定义分页对象
		IPage<Item> iPage = new Page<>(page,rows);
		//根据分页对象执行数据库查询,之后获取其其他分页数据.
		iPage = itemMapper.selectPage(iPage,queryWrapper);
		//获取总记录数
		int total = (int)iPage.getTotal();
		//获取分页后的结果
		List<Item> itemList = iPage.getRecords();
		//封装返回值 返回
		return new EasyUITable(total,itemList);
	}

	@Override
	@Transactional  //注意事务控制   spring一般只能控制运行时异常,检查异常需要手动封装.
	public void saveItem(Item item, ItemDesc itemDesc) {
		//1.默认商品为上架状态
		//item.setStatus(1).setCreated(new Date()).setUpdated(new Date());
		item.setStatus(1);
		itemMapper.insert(item); //先入库之后才有主键,将主键动态的返回.
		//MP支持,用户的操作可以实现自动的主键回显功能.
		//<!--<insert id="" keyProperty="id" keyColumn="id" useGeneratedKeys="true"></insert>-->

		//2.完成商品详情入库操作  要求 item的ID的应该与itemDesc的Id值一致的!!!!
		//知识点: id应该如何获取?
		itemDesc.setItemId(item.getId());
		itemDescMapper.insert(itemDesc);
	}

	@Override
	@Transactional
	public void updateItem(Item item, ItemDesc itemDesc) {
		//更新时需要修改更新时间!!!
		//item.setUpdated(new Date());
		itemMapper.updateById(item);
		//根据itemDescId更新数据
		itemDesc.setItemId(item.getId());
		itemDescMapper.updateById(itemDesc);
	}

	@Override
	@Transactional
	public void deleteItems(Long[] ids) {

		List<Long> longList = Arrays.asList(ids);
		//itemMapper.deleteBatchIds(longList);
		//2.手动完成批量删除的操作....  5分钟
		itemMapper.deleteItems(ids);
		itemDescMapper.deleteBatchIds(longList);
	}

	/**
	 * update(arg1,arg2)
	 * arg1: 需要修改的数据
	 * arg2: 修改的条件构造器
	 * @param status
	 * @param ids
	 */
	@Override
	@Transactional
	public void updateStatus(Integer status, Long[] ids) {
		Item item = new Item();
		item.setStatus(status);
		UpdateWrapper<Item> updateWrapper = new UpdateWrapper<>();
		List<Long> idList = Arrays.asList(ids);
		updateWrapper.in("id",idList);
		itemMapper.update(item,updateWrapper);
	}

	@Override
	public ItemDesc findItemDescById(Long itemId) {

		return itemDescMapper.selectById(itemId);
	}

	/**
	 * 自己手写分页的查询实现数据返回
	 * Sql: select * from tb_item limit 起始位置,展现行数;
	 * 查询第一页   20条
	 * Sql: select * from tb_item limit 0,20;   0-19
	 *查询第二页   20条
	 * Sql: select * from tb_item limit 20,20;  20-39
	 * 查询第三页   20条
	 * Sql: select * from tb_item limit 40,20;
	 * @param page
	 * @param rows
	 * @return
	 */
	/*@Override
	public EasyUITable findItemByPage(Integer page, Integer rows) {
		//1.计算起始位置
		int startIndex = (page-1)*rows;
		List<Item> itemList = itemMapper.findItemByPage(startIndex,rows);

		//2.获取总记录数
		int count = itemMapper.selectCount(null);
		return new EasyUITable(count,itemList);
	}*/
}
