package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.anno.CacheFind;
import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.ItemCat;
import com.jt.util.ObjectMapperUtil;
import com.jt.vo.EasyUITree;
import org.aspectj.weaver.ast.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemCatServiceImpl implements ItemCatService{  //alt +shift+p

    @Autowired(required = false) //jedis对象程序启动时不是必须的
    private Jedis jedis;
    @Autowired
    private ItemCatMapper itemCatMapper;

    @Override
    public String findItemCatNameById(Long itemCatId) { //商品分类名称

        return itemCatMapper.selectById(itemCatId).getName();
    }

    /**
     * 思路:  水果莎拉     进水果   8   80
     *  1.通过parentId查询数据库信息,返回值结果List<ItemCat>
     *  2.将ItemCat信息转化为EasyUITree对象
     *  3.返回的是List<EasyUITree>
     *  用已知实现未知
     *  * @param parentId
     * @return
     */

    //查询数据库的代码
    @Override
    @CacheFind(key="ITEM_CAT_PARENTID",seconds = 100)   //K-V
    public List<EasyUITree> findItemCatList(Long parentId) {

        //1.根据父级分类Id查询数据
        QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id", parentId);
        List<ItemCat> catList = itemCatMapper.selectList(queryWrapper);

        //2.需要将数据进行转化. cartList遍历 封装EasyUITree 添加到集合中即可
        List<EasyUITree> treeList = new ArrayList<>();
        for (ItemCat itemCat: catList){
            Long id = itemCat.getId();
            String text = itemCat.getName();
            //是父级就打开  否则关闭
            String state = itemCat.getIsParent()?"closed":"open";
            EasyUITree easyUITree = new EasyUITree(id, text, state);
            //3.封装返回值数据
            treeList.add(easyUITree);
        }
        return treeList;
    }

    /**
     * 步骤:
     *  先查询Redis缓存  K:V
     *      true   直接返回数据
     *      false  查询数据库
     *
     *  KEY有什么特点: 1.key应该动态变化   2.key应该标识业务属性
     *      key=ITEM_CAT_PARENTID::parentId
     * @param parentId
     * @return
     */
    @Override
    public List<EasyUITree> findItemCache(Long parentId) {
        //记录开始执行的时间
        Long startTime = System.currentTimeMillis();

        //0.定义空集合
        List<EasyUITree> treeList = new ArrayList<>();
        String key = "ITEM_CAT_PARENTID::"+parentId;
        //1.从缓存中查询数据
        String json = jedis.get(key);
        //2.校验JSON中是否有值.
        if(StringUtils.isEmpty(json)){
            //3.如果缓存中没有数据,则查询数据库
            treeList = findItemCatList(parentId);
            //结束时间
            Long entTime = System.currentTimeMillis();
            //4.为了实现缓存处理应该将数据添加到redis中.
            //将数据转化为json结构,保存到redis中
            json = ObjectMapperUtil.toJSON(treeList);
            jedis.set(key, json);
            System.out.println("第一次查询数据库 耗时:"+(entTime-startTime)+"毫秒");
        }else{
            //标识程序有值 将json数据转化为对象即可
            treeList =
                    ObjectMapperUtil.toObject(json,treeList.getClass());
            Long endTime = System.currentTimeMillis();
            System.out.println("查询Redis缓存服务器成功 耗时:"+(endTime-startTime)+"毫秒");
        }
        return treeList;
    }


}
