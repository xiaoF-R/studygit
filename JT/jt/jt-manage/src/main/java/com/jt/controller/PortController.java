package com.jt.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
//Spring能否自动加载
public class PortController {
    /**
     * 通过Spring容器动态获取YML配置文件中的端口即可
     */
    @Value("${server.port}")
    private int port;

    @RequestMapping("/getPort")
    public String getPort(){

        return "当前访问的端口号为:"+port;
    }
}
