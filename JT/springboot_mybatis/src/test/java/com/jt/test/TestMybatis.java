package com.jt.test;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class TestMybatis {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void test01(){

        List<User> userList = userMapper.findAll();
        System.out.println(userList);
    }

    //测试MP方法
    @Test
    public void test02(){
        //查询user表的全部数据
        List<User> userList = userMapper.selectList(null);
        System.out.println(userList);
    }

    /**
     *  1.测试入库
     *  将用户信息  王大锤  20  男 入库
     */

    @Test
    public void insert(){
        User user = new User();
        user.setName("王大锤").setAge(20).setSex("男");
        userMapper.insert(user); //基本告别单表sql
    }

    /**
     *  2.查询联系 查询ID为21的用户
     */
    @Test
    public void select01(){

        User user = userMapper.selectById(21);
        System.out.println(user);
        //查询总记录数
        int count = userMapper.selectCount(null);
        System.out.println(count);
    }

    /**
     * 需求: 查询性别为(=)女 and 年龄大于100岁
     * 条件构造器: 动态拼接where条件的. 多条件中默认的链接符and
     * 常见逻辑运算符
     *  1.eq =   2.gt >   3.lt  <
     *  ge >= ,le  <=
     * */
    @Test
    public void select03(){

        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sex", "女")
                     .gt("age",100);
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    /**
     * 需求: 1.查询名称中包含'精'字的男性  "%精%"
     *       2.查询以精结尾的              %精
     *       3.查询以精开头                精%
     *       * */
    @Test
    public void select04(){

        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.likeLeft("name", "精")
       // queryWrapper.like("name", "精")
                    .eq("sex", "男");
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    /**
     * 需求: 查询sex=女,之后按照age 倒叙排列.如果年龄相同按照id降序排列.
     * */
    @Test
    public void select05(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sex", "女")
                    .orderByDesc("age","id");
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    /**
     * 需求: 查询id为 1,3,5,7,8,9的数据???
     * 关键字  in  or
     * 在关联查询中慎用
     * */
    @Test
    public void select06(){
        Integer[] ids = {1,3,5,7};
       /* List<Integer> idList = new ArrayList<>();
        idList.add(1);
        idList.add(3);
        idList.add(5);
        idList.add(7);
        idList.add(8);*/
        List<Integer> idList = Arrays.asList(ids);
        List<User> userList = userMapper.selectBatchIds(idList);
        System.out.println(userList);
    }

    /**
     * 分析查询测试案例
     * 根据对象中不为null的属性当做操作的要素!!!!
     * 注意事项: POJO 类型必须是包装类型,不能是基本类型.有默认值,影响代码     */
    @Test
    public void testSelect(){
        User user = new User();
        user.setName("王昭君").setAge(19);
        QueryWrapper<User> queryWrapper = new QueryWrapper(user);
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    /**
     * 删除name为null的用户信息
     * sql怎么写 条件构造器怎么加
     * */
    @Test
    public void testDelete(){
        //定义条件构造器
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.isNull("name");
        userMapper.delete(queryWrapper);
    }

    /**
     * 更新操作:
     *  1.将name为null的数据 修改为 name=安琪拉  sex=女   年龄8
     *  参数说明:
     *      1.实体对象   更新的数据内容.
     *      2.条件构造器  动态拼接where条件.
     * */
    @Test
    public void testUpdate(){
        User user = new User();
        user.setName("安琪拉").setSex("女");
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.isNull("name");
        userMapper.update(user,updateWrapper);
    }





}
