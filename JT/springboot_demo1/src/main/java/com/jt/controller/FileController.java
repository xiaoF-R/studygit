package com.jt.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//导入配置文件,之后由Spring容器扫描
@PropertySource(value = "classpath:/properties/image.properties", encoding = "UTF-8")
public class FileController {

    @Value("${image.localDir}") //spel表达式Spring框架所提供的.
    private String localDir;    // = "D:\\JT-SOFT\\images"; //如果放到这里则代码的耦合性高.
    @RequestMapping("/getPath")
    public String getPath(){
        System.out.println("指定图片地址:"+localDir);
        return "图片地址为:"+localDir;
    }
}
